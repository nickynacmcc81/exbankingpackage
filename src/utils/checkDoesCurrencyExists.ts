import CurrencyCodes from 'currency-codes';

export const checkDoesCurrencyExists = (currency: string): boolean =>
  !!CurrencyCodes.code(currency);
