import { v4 as uuid } from 'uuid';
import { Account, CurrencyBalance } from '../types';

export interface AccountRepositoryInterface {
  createAccount(userId: string): Account;
  findById(id: string): Account | null;
  findByUserId(userId: string): Account | null;
  getBalanceByUserIdAndCurrency(userId: string, currency: string): number;
  getAll(): Account[];
  deposit(userId: string, amount: number, currency: string): void;
  withdraw(userId: string, amount: number, currency: string): void;
}

class AccountRepository implements AccountRepositoryInterface {
  private accounts: Account[];

  constructor() {
    this.accounts = [];
  }

  public createAccount(userId: string): Account {
    const account: Account = {
      id: uuid(),
      userId,
      balances: [],
    };
    this.accounts.push(account);
    return account;
  }

  public findById(id: string): Account | null {
    return this.accounts.find((account) => account.id === id) ?? null;
  }

  public findByUserId(userId: string): Account | null {
    return this.accounts.find((account) => account.userId === userId) ?? null;
  }

  public getAll(): Account[] {
    return this.accounts;
  }

  public deposit(userId: string, amount: number, currency: string): void {
    const userAccount = this.findByUserId(userId);

    let balanceForTheGivenCurrency =
      userAccount!.balances.find((balance) => balance.currencyName === currency) ?? null;

    if (!balanceForTheGivenCurrency) {
      // this means that customer does not have balance for the given currency
      balanceForTheGivenCurrency = {
        currencyName: currency,
        balance: amount,
      };
      this.accounts = this.accounts.map((account) => {
        if (account.id === userAccount!.id) {
          return {
            ...account,
            balances: [...account.balances, balanceForTheGivenCurrency as CurrencyBalance],
          };
        }
        return account;
      });
    } else {
      balanceForTheGivenCurrency = {
        ...balanceForTheGivenCurrency,
        balance: balanceForTheGivenCurrency.balance + amount,
      };
      this.accounts = this.accounts.map((account) => {
        if (account.id !== userAccount!.id) {
          return account;
        }
        return {
          ...account,
          balances: account.balances.map((balance) => {
            if (balance.currencyName === currency) {
              return balanceForTheGivenCurrency as CurrencyBalance;
            }
            return balance;
          }),
        };
      });
    }
  }

  public getBalanceByUserIdAndCurrency(userId: string, currency: string): number {
    const account = this.findByUserId(userId);
    if (!account) return 0;
    const currencyBalance = account.balances.find((balance) => balance.currencyName === currency);
    return currencyBalance?.balance ?? 0;
  }

  public withdraw(userId: string, amount: number, currency: string): void {
    this.accounts = this.accounts.map((account) => {
      if (account.userId === userId) {
        return {
          ...account,
          balances: account.balances.map((currencyBalance) => {
            if (currencyBalance.currencyName === currency) {
              return {
                ...currencyBalance,
                balance: currencyBalance.balance - amount,
              };
            }
            return currencyBalance;
          }),
        };
      }
      return account;
    });
  }
}

export default new AccountRepository();
