import { UserRepositoryInterface } from '../repository/user';
import { ResponseOk, UserAlreadyExistsError, BankingErrors, User } from '../types';
import AccountRepository from '../repository/account';

export interface UserServiceInterface {
  createUser(username: string): ResponseOk | UserAlreadyExistsError;
  getAll(): User[];
  doesUserExists(username: string): boolean;
  findByUsername(username: string): User | null;
}

export class UserService implements UserServiceInterface {
  private repository: UserRepositoryInterface;

  constructor(repository: UserRepositoryInterface) {
    this.repository = repository;
  }

  public createUser(username: string): ResponseOk | UserAlreadyExistsError {
    if (this.doesUserExists(username)) {
      return {
        success: false,
        message: BankingErrors.UserAlreadyExists,
      };
    }
    const user = this.repository.create(username);
    AccountRepository.createAccount(user.id);
    return { success: true };
  }

  public getAll(): User[] {
    return this.repository.getAll();
  }

  public doesUserExists(username: string): boolean {
    const user = this.repository.findByUsername(username);
    return !!user;
  }

  public findByUsername(username: string): User | null {
    return this.repository.findByUsername(username);
  }
}
